package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func readLineInput(message string) (string, error) {
	fmt.Printf("%s: ", message)
	reader := bufio.NewReader(os.Stdin)
	input, err := reader.ReadString('\n')
	input = strings.TrimSpace(input)
	return input, err
}

func getStateKey(state_name string) string {
	state_name = strings.ToLower(strings.TrimSpace(state_name))
	words := strings.Fields(state_name)
	state_name = strings.Join(words, "_")
	return state_name
}

func main() {
	const stateFilePath string = "./states.json"
	const menu string = `Menu:
	0. Quit
	1. Read States
	2. Get State Info
	3. Add State
	4. Remove State
	5. Modify State
		a. Add district
		b. Remove district
		c. Change type
	6. Show Menu`

	states, err := readStates(stateFilePath)
	if err != nil {
		log.Fatal("error while reading to file", err)
	}

	fmt.Println(menu)

	choice := -1

	for choice != 0 {
		isModified := false
		fmt.Print("\nEnter choice: ")
		fmt.Scan(&choice)

		switch choice {

		// quiting
		case 0:
			fmt.Println("Quiting...")

		// list all states
		case 1:
			fmt.Printf("\nFound %v states\n", len(states))
			for _, state := range states {
				fmt.Println("---------------")
				state.printState()
			}

		// get state info
		case 2:
			state_name, err := readLineInput("Enter state name")
			if err != nil {
				fmt.Println("Failed to read the state name")
				continue
			}
			state_name = getStateKey(state_name)
			if state, exists := states[state_name]; exists {
				state.printState()
			} else {
				fmt.Println("No state found named", state_name)
			}

		// add state
		case 3:
			state_name, err := readLineInput("Enter state name")
			if err != nil {
				fmt.Println("Failed to read the state name")
			}
			state_key := getStateKey(state_name)
			if _, exists := states[state_key]; exists {
				fmt.Println("State already exists!")
				continue
			}
			isUT := false
			ut_input, err := readLineInput("Is it UT? - yes/no")
			if err != nil {
				fmt.Println("Failed to is UT input")
				continue
			}
			if strings.ToLower(ut_input) == "yes" {
				isUT = true
			}
			district_input, err := readLineInput("Enter districts to be added - separated by a comma")
			if err != nil {
				fmt.Println("Failed to read districts")
				continue
			}
			districts := strings.Split(district_input, ",")
			state := NewState(state_name, isUT, districts)
			states[state_key] = *state
			isModified = true

		// remove state
		case 4:
			state_name, err := readLineInput("Enter state name")
			if err != nil {
				fmt.Println("Failed to read the state name")
				continue
			}
			state_name = getStateKey(state_name)
			if _, exists := states[state_name]; exists {
				delete(states, state_name)
				isModified = true
			} else {
				fmt.Println("No state found named", state_name)
			}

		// modify state
		case 5:
			var subChoice string
			state_name, err := readLineInput("Enter state name")
			if err != nil {
				fmt.Println("Failed to read the state name")
				continue
			}
			state_name = getStateKey(state_name)
			state, exists := states[state_name]
			if !exists {
				fmt.Println("No state found named ", state_name)
				continue
			}

			fmt.Print("Enter sub-choice: ")
			fmt.Scan(&subChoice)

			// sub choices
			switch subChoice {
			// add state
			case "a":
				district_input, err := readLineInput("Enter districts to be added - separated by a comma")
				if err != nil {
					fmt.Println("Failed to read districts")
					continue
				}
				districts := strings.Split(district_input, ",")
				state.addDistricts(districts...)
				states[state_name] = state
				isModified = true
				fmt.Println(state)
			// remove district
			case "b":
				district_input, err := readLineInput("Enter district to be removed")
				if err != nil {
					fmt.Println("Failed to read district")
					continue
				}
				state.removeDistrict(district_input)
				states[state_name] = state
				isModified = true
				fmt.Println(state)
			// change type
			case "c":
				state.changeStateType()
				states[state_name] = state
				isModified = true
				fmt.Println(state)
			default:
				fmt.Println("Invalid choice to modify state!")
			}

		// menu
		case 6:
			fmt.Println(menu)

		default:
			fmt.Println("Invalid choice!")
		}

		if isModified {
			err := writeStates(stateFilePath, states)
			if err != nil {
				log.Fatal("error while writing to file", err)
			}
		}
	}
}
