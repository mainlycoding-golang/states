package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
)

func readStates(filePath string) (map[string]State, error) {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return nil, errors.New("error while opening file")
	}
	var states map[string]State
	err = json.Unmarshal(content, &states)
	if err != nil {
		return nil, errors.New("can't unmarshal states")
	}
	return states, nil
}

func writeStates(filePath string, states map[string]State) error {
	jsonData, err := json.Marshal(states)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filePath, jsonData, 0644)
	return err
}
