package main

import (
	"fmt"
	"strings"
)

type State struct {
	Name      string   `json:"name"`
	IsUT      bool     `json:"isUT"`
	Districts []string `json:"districts"`
}

func NewState(name string, isUT bool, districts []string) *State {
	state := State{Name: name, IsUT: isUT, Districts: districts}
	return &state
}

func (state *State) findDistrict(district string) int {
	for idx, item := range state.Districts {
		if district == item {
			return idx
		}
	}
	return -1
}

func (state *State) addDistricts(districts ...string) {
	for _, district := range districts {
		district = strings.TrimSpace(district)
		if state.findDistrict(district) == -1 {
			state.Districts = append(state.Districts, district)
		}
	}
}

func (state *State) removeDistrict(district string) error {
	district = strings.TrimSpace(district)
	idx := state.findDistrict(district)
	if idx == -1 {
		return fmt.Errorf("no district found named %s", district)
	}
	state.Districts[idx] = state.Districts[len(state.Districts)-1]
	state.Districts = state.Districts[:len(state.Districts)-1]
	return nil
}

func (state *State) changeStateType() {
	state.IsUT = !state.IsUT
}

func (state *State) printState() {
	fmt.Println("State Name:", state.Name)
	fmt.Println("Is Union Territory:", state.IsUT)
	fmt.Println("Districts:", state.Districts)
}
